# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetProfileTest do
  use ExUnit.Case

  test "GET profile/{user_id}" do
    endpoint = %Polyjuice.Client.Endpoint.GetProfile{
      user_id: "@alice:kazarma.local"
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: "",
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"}
             ],
             method: :get,
             path: "_matrix/client/r0/profile/%40alice%3Akazarma.local"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             "[{\"avatar_url\": \"yoyo\", \"displayname\": \"polo\"}]"
           ) == {:ok, [%{"avatar_url" => "yoyo", "displayname" => "polo"}]}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
