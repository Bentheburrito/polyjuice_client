# Copyright 2021 Ketsapiwiq <ketsapiwiq@protonmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostUserDirectorySearchTest do
  use ExUnit.Case

  test "POST search with limit" do
    endpoint = %Polyjuice.Client.Endpoint.PostUserDirectorySearch{
      search_term: "@alice:example.com",
      limit: 10
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert TestUtil.http_spec_body_to_binary(http_spec) ==
             %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: ~s({"limit":10,"search_term":"@alice:example.com"}),
               path: "_matrix/client/r0/user_directory/search",
               headers: [
                 {"Accept", "application/json"},
                 {"Accept-Encoding", "gzip, deflate"},
                 {"Content-Type", "application/json"}
               ],
               method: :post
             }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({
              "results": [
                {
                  "user_id": "@alice:example.com",
                  "display_name": "Alice",
                  "avatar_url": "mxc://bar.com/foo"
                }
              ],
              "limited": false
            })
           ) ==
             {:ok,
              %{
                "results" => [
                  %{
                    "avatar_url" => "mxc://bar.com/foo",
                    "display_name" => "Alice",
                    "user_id" => "@alice:example.com"
                  }
                ],
                "limited" => false
              }}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end

  test "POST search without limit" do
    endpoint = %Polyjuice.Client.Endpoint.PostUserDirectorySearch{
      search_term: "@alice:example.com",
      limit: nil
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert TestUtil.http_spec_body_to_binary(http_spec) ==
             %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: ~s({"search_term":"@alice:example.com"}),
               method: :post,
               headers: [
                 {"Accept", "application/json"},
                 {"Accept-Encoding", "gzip, deflate"},
                 {"Content-Type", "application/json"}
               ],
               path: "_matrix/client/r0/user_directory/search"
             }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             ~s({
              "results": [
                {
                  "user_id": "@alice:example.com",
                  "display_name": "Alice",
                  "avatar_url": "mxc://bar.com/foo"
                }
              ],
              "limited": false
            })
           ) ==
             {:ok,
              %{
                "results" => [
                  %{
                    "avatar_url" => "mxc://bar.com/foo",
                    "display_name" => "Alice",
                    "user_id" => "@alice:example.com"
                  }
                ],
                "limited" => false
              }}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
