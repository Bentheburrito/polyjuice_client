# Copyright 2021 Arjan Scherpenisse <arjan@miraclethings.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsTypingTest do
  use ExUnit.Case

  test "PUT rooms typing" do
    endpoint = %Polyjuice.Client.Endpoint.PutRoomsTyping{
      room: "!room_id",
      user_id: "@alice:homeserver.local",
      typing: true,
      timeout: 1000
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint)

    assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: "{\"timeout\":1000,\"typing\":true}",
             headers: [
               {"Accept", "application/json"},
               {"Accept-Encoding", "gzip, deflate"},
               {"Content-Type", "application/json"}
             ],
             method: :put,
             path: "_matrix/client/r0/rooms/%21room_id/typing/%40alice%3Ahomeserver.local"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [{"Content-Type", "application/json"}],
             "{}"
           ) == :ok

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) ==
             {:error, 500, %{"body" => "Aaah!", "errcode" => "CA_UHOREG_POLYJUICE_BAD_RESPONSE"}}
  end
end
