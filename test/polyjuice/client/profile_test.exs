# Copyright 2020 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.ProfileTest do
  use ExUnit.Case
  doctest Polyjuice.Client.Profile

  test "get_profile" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfile{
               user_id: "@amelie:example.org"
             },
             {:ok, %{avatar_url: "mxc://matrix.org/SDGdghriugerRg", displayname: "poulain"}}
           }
         } do
      {:ok, data} = Polyjuice.Client.Profile.get_profile(client, "@amelie:example.org")
      assert data[:displayname] == "poulain"
      assert data[:avatar_url] == "mxc://matrix.org/SDGdghriugerRg"
    end
  end

  test "get_profile without user_id" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfile{
               user_id: "@alice:example.org"
             },
             {:ok, %{avatar_url: "mxc://matrix.org/SDGdghriugerRg", displayname: "poulain"}}
           }
         } do
      {:ok, data} = Polyjuice.Client.Profile.get_profile(client)
      assert data[:displayname] == "poulain"
      assert data[:avatar_url] == "mxc://matrix.org/SDGdghriugerRg"
    end
  end

  test "get avatar url" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfileAvatarUrl{
               user_id: "@alice:example.org"
             },
             {:ok, "mxc://matrix.org/wefh34uihSDRGhw34"}
           }
         } do
      {:ok, avatar_url} = Polyjuice.Client.Profile.get_avatar_url(client, "@alice:example.org")
      assert avatar_url == "mxc://matrix.org/wefh34uihSDRGhw34"
    end
  end

  test "get avatar url without user_id" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfileAvatarUrl{
               user_id: "@alice:example.org"
             },
             {:ok, "mxc://matrix.org/wefh34uihSDRGhw34"}
           }
         } do
      {:ok, avatar_url} = Polyjuice.Client.Profile.get_avatar_url(client)
      assert avatar_url == "mxc://matrix.org/wefh34uihSDRGhw34"
    end
  end

  test "modify avatar url" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutProfileAvatarUrl{
               user_id: "@alice:example.org",
               avatar_url: "mxc://matrix.org/wefh34uihSDRGhw34"
             },
             {:ok}
           }
         } do
      {:ok} =
        Polyjuice.Client.Profile.put_avatar_url(
          client,
          "@alice:example.org",
          "mxc://matrix.org/wefh34uihSDRGhw34"
        )
    end
  end

  test "modify avatar url without user_id" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutProfileAvatarUrl{
               user_id: "@alice:example.org",
               avatar_url: "mxc://matrix.org/wefh34uihSDRGhw34"
             },
             {:ok}
           }
         } do
      {:ok} =
        Polyjuice.Client.Profile.put_avatar_url(
          client,
          "mxc://matrix.org/wefh34uihSDRGhw34"
        )
    end
  end

  test "get displayname" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfileDisplayname{
               user_id: "@alice:example.org"
             },
             {:ok, "alice"}
           }
         } do
      {:ok, displayname} = Polyjuice.Client.Profile.get_displayname(client, "@alice:example.org")
      assert displayname == "alice"
    end
  end

  test "get displayname wihout user_id" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.GetProfileDisplayname{
               user_id: "@alice:example.org"
             },
             {:ok, "alice"}
           }
         } do
      {:ok, displayname} = Polyjuice.Client.Profile.get_displayname(client)
      assert displayname == "alice"
    end
  end

  test "modify displayname" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutProfileDisplayname{
               user_id: "@alice:example.org",
               displayname: "marie"
             },
             {:ok}
           }
         } do
      {:ok} = Polyjuice.Client.Profile.put_displayname(client, "@alice:example.org", "marie")
    end
  end

  test "modify displayname without user_id" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PutProfileDisplayname{
               user_id: "@alice:example.org",
               displayname: "marie"
             },
             {:ok}
           }
         } do
      {:ok} = Polyjuice.Client.Profile.put_displayname(client, "marie")
    end
  end
end
