# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.MsgBuilder do
  alias Polyjuice.Client.MsgBuilder, as: MsgBuilder

  @moduledoc """
  Build a message out of composable parts.
  """

  @doc ~S"""
  Escape special HTML characters.

  Returns an iodata.

  Examples:

      iex> Polyjuice.Client.MsgBuilder.html_escape("a<>&\"", false)
      ...> |> IO.iodata_to_binary()
      "a&lt;&gt;&amp;\""

      iex> Polyjuice.Client.MsgBuilder.html_escape("a<>&b\"", true)
      ...> |> IO.iodata_to_binary()
      "a&lt;&gt;&amp;b&quot;"
  """
  @spec html_escape(str :: String.t(), escape_quotes :: boolean) :: iodata
  def html_escape("", esc_q) when is_boolean(esc_q), do: ""

  for {char, seq} <- Enum.zip('<>&', ["lt", "gt", "amp"]) do
    def html_escape(<<unquote(char)>> <> rest, esc_q) when is_boolean(esc_q) do
      [unquote("&" <> seq <> ";") | html_escape(rest, esc_q)]
    end
  end

  def html_escape(<<?">> <> rest, true) do
    ["&quot;" | html_escape(rest, true)]
  end

  def html_escape(str, esc_q) when is_boolean(esc_q) do
    size = chunk_size(str, esc_q, 0)
    <<chunk::binary-size(size), rest::binary>> = str
    [chunk | html_escape(rest, esc_q)]
  end

  defp chunk_size("", _, acc), do: acc

  defp chunk_size(<<c>> <> _, _, acc) when c in '<>&', do: acc

  defp chunk_size(<<?">> <> _, true, acc), do: acc

  defp chunk_size(<<_>> <> rest, esc_q, acc), do: chunk_size(rest, esc_q, acc + 1)

  defprotocol MsgData do
    @moduledoc """
    Something that can be turned into a message.
    """

    @doc """
    Return the plain-text version of the message part, and whether the HTML
    version will be different.

    Returns a tuple of the form `{plain_text_version, html_differs}`, e.g.
    `{"*Hello* /world/", true}`.
    """
    def to_text(msg)

    @doc """
    Return the HTML version of the message part.
    """
    def to_html(msg)
  end

  defimpl Polyjuice.Client.MsgBuilder.MsgData, for: List do
    def to_text([]), do: {"", false}

    def to_text([head | tail]) do
      {head_text, head_html_differs} = MsgData.to_text(head)
      {tail_text, tail_html_differs} = MsgData.to_text(tail)
      {[head_text | tail_text], head_html_differs or tail_html_differs}
    end

    def to_html([]), do: ""

    def to_html([head | tail]) do
      [MsgData.to_html(head) | MsgData.to_html(tail)]
    end
  end

  defimpl Polyjuice.Client.MsgBuilder.MsgData, for: BitString do
    def to_text(str), do: {str, false}

    def to_html(str) do
      MsgBuilder.html_escape(str, false)
      |> IO.iodata_to_binary()
      |> String.replace("\n", "<br />\n")
    end
  end

  defimpl Polyjuice.Client.MsgBuilder.MsgData, for: Tuple do
    def to_text({text, _}) when is_binary(text) or is_list(text), do: {text, true}

    def to_html({_, html}) when is_binary(html) or is_list(html), do: html
  end

  defmodule Link do
    @moduledoc false
    defstruct [:href, :contents]

    defimpl Polyjuice.Client.MsgBuilder.MsgData do
      def to_text(%{href: href, contents: contents}) do
        {contents_text, _} = MsgData.to_text(contents)

        {
          [?[, contents_text, "](", href, ?)],
          true
        }
      end

      def to_html(%{href: href, contents: contents}) do
        [
          "<a href=\"",
          MsgBuilder.html_escape(href, true),
          "\">",
          MsgData.to_html(contents),
          "</a>"
        ]
      end
    end
  end

  @doc ~S"""
  Generate a Matrix message contents from message data.

  Examples:

      iex> Polyjuice.Client.MsgBuilder.to_message("foo")
      %{"msgtype" => "m.text", "body" => "foo"}

      iex> Polyjuice.Client.MsgBuilder.to_message(["foo", "bar"])
      %{"msgtype" => "m.text", "body" => "foobar"}
  """
  @spec to_message(
          msgdata :: Polyjuice.Client.MsgBuilder.MsgData.t(),
          msgtype :: String.t()
        ) :: Polyjuice.Util.event_content()
  def to_message(msgdata, msgtype \\ "m.text") do
    {html, html_differs} = MsgData.to_text(msgdata)
    body = IO.iodata_to_binary(html) |> String.trim("\n")

    if html_differs do
      %{
        "msgtype" => msgtype,
        "body" => body,
        "format" => "org.matrix.custom.html",
        "formatted_body" => MsgData.to_html(msgdata) |> IO.iodata_to_binary()
      }
    else
      %{
        "msgtype" => msgtype,
        "body" => body
      }
    end
  end

  @doc ~S"""
  Mention a user.

  Examples:

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.mention("@alice:example.com", "Alice")
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "Alice",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<a href=\"https://matrix.to/#/@alice:example.com\">Alice</a>"
      }

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.mention("@alice:example.com")
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "@alice:example.com",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<a href=\"https://matrix.to/#/@alice:example.com\">@alice:example.com</a>"
      }
  """
  @spec mention(user_id :: String.t(), display_name :: String.t() | nil) :: MsgData.t()
  def mention(user_id, display_name \\ nil)

  def mention(user_id, nil) when is_binary(user_id) do
    mention(user_id, user_id)
  end

  def mention(user_id, display_name)
      when is_binary(user_id) and is_binary(display_name) do
    {
      display_name,
      [
        "<a href=\"https://matrix.to/#/",
        MsgBuilder.html_escape(URI.encode(user_id), true),
        "\">",
        MsgBuilder.html_escape(display_name, false),
        "</a>"
      ]
    }
  end

  @doc ~S"""
  Create an image.

  Examples:

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.image("mxc://example.com/foo")
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "[mxc://example.com/foo]",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<img src=\"mxc://example.com/foo\" />"
      }

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.image("mxc://example.com/foo", alt: "some image")
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "some image",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<img src=\"mxc://example.com/foo\" alt=\"some image\" />"
      }

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.image(
      ...>     "mxc://example.com/foo", alt: "some image", width: 100
      ...>   )
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "some image",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<img src=\"mxc://example.com/foo\" alt=\"some image\" width=\"100\" />"
      }
  """
  @spec image(src :: String.t(), attrs :: list()) :: MsgData.t()
  def image(src, attrs \\ []) when is_binary(src) and is_list(attrs) do
    alt = Keyword.get(attrs, :alt)
    text = if alt != nil, do: alt, else: [?[, src, ?]]

    html = [
      "<img src=\"",
      MsgBuilder.html_escape(src, true),
      "\" ",
      Enum.map(attrs, fn {name, value} ->
        [
          MsgBuilder.html_escape(to_string(name), true),
          "=\"",
          MsgBuilder.html_escape(to_string(value), true),
          "\" "
        ]
      end),
      "/>"
    ]

    {text, html}
  end

  @doc ~S"""
  Create a link.

  Examples:

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.link(
      ...>     Polyjuice.Client.MsgBuilder.image(
      ...>       "mxc://example.com/foo", alt: "some image", width: 100
      ...>     ),
      ...>     "https://matrix.org/"
      ...>   )
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "[some image](https://matrix.org/)",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<a href=\"https://matrix.org/\"><img src=\"mxc://example.com/foo\" alt=\"some image\" width=\"100\" /></a>"
      }
  """
  @spec link(contents :: MsgData.t(), href :: String.t()) :: MsgData.t()
  def link(contents, href) when is_binary(href) do
    %Link{href: href, contents: contents}
  end

  @doc ~S"""
  Create a mathematical expression using LaTeX format.

  Note: This uses the format defined in
  [MSC2191](https://github.com/matrix-org/matrix-doc/pull/2191), which is not
  part of the Matrix spec (yet).

  Examples:

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.latex("x^2")
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "$x^2$",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<span data-mx-maths=\"x^2\"><code>x^2</code></span>"
      }

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.latex("x^2", true)
      ...> )
      %{
        "msgtype" => "m.text",
        "body" => "$$\nx^2\n$$",
        "format" => "org.matrix.custom.html",
        "formatted_body" => "<div data-mx-maths=\"x^2\"><code>x^2</code></div>"
      }
  """
  @spec latex(expression :: String.t(), display :: boolean) :: MsgData.t()
  def latex(expression, display \\ false) when is_binary(expression) and is_boolean(display) do
    {elem, delim} = if display, do: {"div", "\n$$\n"}, else: {"span", ?$}

    {
      [delim, expression, delim],
      [
        ?<,
        elem,
        " data-mx-maths=\"",
        html_escape(expression, true),
        "\"><code>",
        html_escape(expression, true),
        "</code></",
        elem,
        ">"
      ]
    }
  end

  @doc ~S"""
  Colours text as a rainbow.

  Example:

      iex> Polyjuice.Client.MsgBuilder.to_message(
      ...>   Polyjuice.Client.MsgBuilder.rainbow("Hello, world!")
      ...> )
      %{
        "body" => "🌈Hello, world!🌈",
        "format" => "org.matrix.custom.html",
        "formatted_body" => ~s(<font color="#FF00BE">H</font><font color="#FF0055">e</font><font color="#FF7200">l</font><font color="#F6AB00">l</font><font color="#92CB00">o</font><font color="#00DC00">,</font><font color="#00E47C"> </font><font color="#00E7EE">w</font><font color="#00E6FF">o</font><font color="#00DCFF">r</font><font color="#00C1FF">l</font><font color="#BE8EFF">d</font><font color="#FF21FF">!</font>),
        "msgtype" => "m.text"
      }
  """
  # the rainbow calculations are stolen from matrix-react-sdk:src/utils/colour.ts,
  # by ginnythecat@lelux.net
  @spec rainbow(text :: String.t()) :: MsgData.t()
  def rainbow(text) do
    graphemes = String.graphemes(text)
    length = Enum.count(graphemes)
    frequency = 2 * :math.pi() / length

    {
      ["🌈", text, "🌈"],
      graphemes
      |> Enum.with_index()
      |> Enum.map(fn {t, idx} ->
        {a, b} = generate_ab(idx * frequency, 1)
        {red, green, blue} = lab_to_rgb(75, a, b)

        [
          "<font color=\"#",
          Integer.to_string(red, 16) |> String.pad_leading(2, "0"),
          Integer.to_string(green, 16) |> String.pad_leading(2, "0"),
          Integer.to_string(blue, 16) |> String.pad_leading(2, "0"),
          "\">",
          t,
          "</font>"
        ]
      end)
    }
  end

  defp generate_ab(hue, chroma) do
    {chroma * 127 * :math.cos(hue), chroma * 127 * :math.sin(hue)}
  end

  defp lab_to_rgb(l, a, b) do
    y = (l + 16) / 116
    x = adjust_xyz(y + a / 500) * 0.9505
    z = adjust_xyz(y - b / 200) * 1.0899

    y = adjust_xyz(y)

    red = 3.24096994 * x - 1.53738318 * y - 0.49861076 * z
    green = -0.96924364 * x + 1.8759675 * y + 0.04155506 * z
    blue = 0.05563008 * x - 0.20397696 * y + 1.05697151 * z

    {adjust_rgb(red), adjust_rgb(green), adjust_rgb(blue)}
  end

  defp adjust_xyz(v) do
    if v > 0.2069 do
      :math.pow(v, 3)
    else
      0.1284 * v - 0.01771
    end
  end

  defp gamma_correction(v) do
    if v <= 0.0031308 do
      12.92 * v
    else
      1.055 * :math.pow(v, 1 / 2.4) - 0.055
    end
  end

  defp adjust_rgb(v) do
    v = round(gamma_correction(v) * 255)

    cond do
      v < 0 -> 0
      v > 255 -> 255
      true -> v
    end
  end
end
