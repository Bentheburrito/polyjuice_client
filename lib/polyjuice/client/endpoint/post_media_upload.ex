# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostMediaUpload do
  @moduledoc """
  Upload to the media repository.

  https://matrix.org/docs/spec/client_server/latest#post-matrix-media-r0-upload
  """

  @type t :: %__MODULE__{
          filename: String.t(),
          data: binary | {:file, String.t()},
          mimetype: String.t()
        }

  @enforce_keys [:filename, :data, :mimetype]
  defstruct [
    :filename,
    :data,
    mimetype: "application/octet-stream"
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%{
          filename: filename,
          data: data,
          mimetype: mimetype
        }) do
      Polyjuice.Client.Endpoint.HttpSpec.post(
        :media_r0,
        "upload",
        headers: [
          {"Accept", "application/json"},
          {"Content-Type", mimetype}
        ],
        query: [filename: filename],
        body: data
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "content_uri")}
    end
  end
end
