# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PutRoomsState do
  @moduledoc """
  Send a state event to a room.

  https://matrix.org/docs/spec/client_server/latest#put-matrix-client-r0-rooms-roomid-state-eventtype-statekey
  """

  @type t :: %__MODULE__{
          room: String.t(),
          event_type: String.t(),
          state_key: String.t(),
          content: map
        }

  @enforce_keys [:room, :event_type, :state_key, :content]
  defstruct [
    :room,
    :event_type,
    :state_key,
    :content
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.PutRoomsState{
          room: room,
          event_type: event_type,
          state_key: state_key,
          content: content
        }) do
      e = &URI.encode_www_form/1
      body = Jason.encode_to_iodata!(content)

      Polyjuice.Client.Endpoint.HttpSpec.put(
        :r0,
        "rooms/#{e.(room)}/state/#{e.(event_type)}/#{e.(state_key)}",
        body: body
      )
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "event_id")}
    end
  end
end
