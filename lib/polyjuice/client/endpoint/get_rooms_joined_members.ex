# Copyright 2021 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomsJoinedMembers do
  @moduledoc """
  Get a map of MXIDs to member info objects for members of the room.
  Returns a map of MXIDs to member info objects for members of the room of the type:

  ```
  %{
    MXIDs => %{
      "display_name" => "alice",
      "avatar_url" => "mxc://example.org/aabbccddeeffgghh"
    }
  }
  ```

  https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-rooms-roomid-joined-members
  """

  @type t :: %__MODULE__{
          room: String.t()
        }

  @enforce_keys [:room]
  defstruct [
    :room
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.GetRoomsJoinedMembers{
          room: room
        }) do
      e = &URI.encode_www_form/1

      path = "rooms/#{e.(room)}/joined_members"

      Polyjuice.Client.Endpoint.HttpSpec.get(:r0, path)
    end

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end
  end

  defimpl Polyjuice.Client.Endpoint.BodyParser do
    def parse(_req, parsed) do
      {:ok, Map.get(parsed, "joined")}
    end
  end
end
