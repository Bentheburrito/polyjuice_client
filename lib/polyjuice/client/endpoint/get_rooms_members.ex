# Copyright 2021 Multi Prise <multiestunhappydev@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomsMembers do
  @moduledoc """
  Get the list of members for this room.
  Return the full lists of membership events

  https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-members
  """

  @type t :: %__MODULE__{
          room: String.t(),
          at: String.t() | nil,
          membership: :join | :invite | :leave | :ban | nil,
          not_membership: :join | :invite | :leave | :ban | nil
        }

  @enforce_keys [:room]
  defstruct [
    :room,
    :at,
    :membership,
    :not_membership
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(%Polyjuice.Client.Endpoint.GetRoomsMembers{
          room: room,
          at: at,
          membership: membership,
          not_membership: not_membership
        }) do
      e = &URI.encode_www_form/1

      path = "rooms/#{e.(room)}/members"

      body =
        [
          if(not is_nil(at), do: [{"at", at}], else: []),
          if(not is_nil(membership), do: [{"membership", membership}], else: []),
          if(not is_nil(not_membership), do: [{"not_membership", not_membership}], else: [])
        ]
        |> Enum.concat()
        |> Map.new()
        |> Jason.encode_to_iodata!()

      Polyjuice.Client.Endpoint.HttpSpec.get(
        :r0,
        path,
        body: body,
        auth_required: true
      )
    end

    require Logger

    def transform_http_result(req, status_code, resp_headers, body) do
      Polyjuice.Client.Endpoint.parse_response(req, status_code, resp_headers, body)
    end

    defimpl Polyjuice.Client.Endpoint.BodyParser do
      def parse(_req, parsed) do
        {:ok, Map.get(parsed, "chunk")}
      end
    end
  end
end
