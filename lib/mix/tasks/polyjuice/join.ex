# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Mix.Tasks.Polyjuice.Join do
  @moduledoc """
  Join a room.

      mix polyjuice.join [opts] homeserver_url room_id_or_alias

  ## Command line options

  * `--storage` - Elixir code to create the storage to fetch the client state
    from.
  * `--access-token` - The access token to log out.
  * `--servers` - A comma-separated list of servers to use to try to join the room.

  At least one of `--storage` or `--access-token` must be provided.

  """
  @shortdoc "Join a Matrix room."
  use Mix.Task

  @impl Mix.Task
  def run(args) do
    Mix.Task.run("app.start", [])

    with {opts, [url, room]} <-
           OptionParser.parse!(args,
             strict: [
               storage: :string,
               access_token: :string,
               servers: :string
             ]
           ) do
      storage = opts[:storage]
      access_token = opts[:access_token]

      if {storage, access_token} == {nil, nil} do
        Mix.Task.run("help", ["polyjuice.join"])
      else
        storage =
          if is_binary(storage) do
            Code.eval_string(storage) |> (fn {x, _} -> x end).()
          else
            nil
          end

        servers =
          case Keyword.get(opts, :servers, "") do
            "" -> []
            s -> String.split(s, ",")
          end

        {:ok, client_pid} =
          Polyjuice.Client.start_link(
            url,
            access_token: opts[:access_token],
            storage: storage,
            sync: false
          )

        client = Polyjuice.Client.get_client(client_pid)

        ret = Polyjuice.Client.Room.join(client, room, servers)

        Polyjuice.Client.API.stop(client)

        if storage, do: Polyjuice.Client.Storage.close(storage)

        case ret do
          {:ok, room_id} ->
            IO.puts("Successfully joined #{room_id}.")

          _ ->
            IO.puts("Join failed: #{inspect(ret)}.")
        end
      end
    else
      _ ->
        Mix.Task.run("help", ["polyjuice.join"])
    end
  end
end
